<?php /* Template Name: Products Template */ get_header(); ?>

<div class="container break">

</div>

<div class="container" >
    <div class="row">
        <div class="col-md-3 text-center">
            <ul class="list-group menu-left">
                <li class="katalog list-group-item ">
                    <?php 
                        $lang = pll_current_language('slug'); 
                        if($lang == 'pl') { 
                    ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog.pdf') ?>"><p class="p1"><?php _e('POBIERZ NOWY') ?></p><p class="p2"><?php _e('KATALOG') ?></p> </a>
                    <?php } if($lang == 'fr') {  ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog_fr.pdf') ?>"><p class="p1"><?php _e('NOUVELLE') ?></p><p class="p2"><?php _e('OFFRE') ?></p> </a>
                    <?php } if($lang == 'en') { ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog_en.pdf') ?>"><p class="p1"><?php _e('NEW') ?></p><p class="p2"><?php _e('OFFER') ?></p> </a>
                    <?php } ?>
                    <div class="show-menu2">
                        <img src="<?php echo DEREN_THEME_URL ?>img/pdf_download.png" class="" alt=""/>
                    </div>
                </li>
                <?php 
                	if($lang == 'fr') 
                		include('elements/menu-left-fr.php'); 
                	elseif($lang == 'en') 
                		include('elements/menu-left-en.php'); 
                	else
                		include('elements/menu-left.php'); 
               	?>
            </ul>
        </div>
        <div class="col-md-3 text-index">
            <div class="content-label text-center">
                <p class="h44">
                    <?php
                    $pagename = get_query_var('pagename');
                    if ( !$pagename && $id > 0 ) {
                        // If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
                        $post = $wp_query->get_queried_object();
                        $pagename = $post->post_name;
                    }
                    $title= $pagename;



                    $tradi='tradi';
                    $classic='classic';
                    $design='design';
                    $wew='wewnętrzne';

                    $klas='klasyczne';
                    $fra='system-francuski';
                    $eng='system-angielski';

                    if ($title==$tradi || $title==$classic || $title==$design || $title==$wew ):
                        echo 'DRZWI';
                    elseif ($title==$klas || $title==$fra || $title==$eng):
                        echo 'OKNA';
                    else:
                        echo '';
                    endif;
                    ?>
                </p>
                <p class="h11" style="line-height: 32px;"><strong><?php the_title(); ?></strong></p>
            </div>
        </div>
        <div class="col-md-6">
            <br>
            <div class="content-label2">
            </div>
        </div>
        <div class="col-md-9 text-index">
            <div class="products">
                    <?php
                    if( have_rows('produkty') ):

                        // loop through the rows of data
                        while ( have_rows('produkty') ) : the_row();

                            $link = get_sub_field('produkt_szczegoly');
                            $image = get_sub_field('miniatura')['sizes']['medium'];
                            $title = get_sub_field('tytul');


                            echo '
                            <div class="product">
                                <a href="'.get_site_url().'/produkty/'.$link->post_name.'" title="'.$title.'">
                                    <div class="product-image">
                                        <img src="'.$image.'" alt="">
                                    </div>
                                    <div class="product-title">
                                        '.$title.'
                                    </div>
                                </a>
                            </div>
                            ';
                        endwhile;

                    endif;

                    ?>
            </div>

            <style>
                .product {
                    border: 1px solid #cdcdcd;
                    height: 225px;
                    margin: 4% 2%;
                    padding: 5px 10px 0;
                    width: 21%;
                    float: left;
                }

                .product-image {
                    height: 175px;
                    text-align: center;
                    align-items: center;
                    display: flex;
                    justify-content: center;
                }

                .product-image img {
                    max-width: 80%;
                    max-height: 160px;
                }

                .product-title {
                     color: #4f4f4f;
                    align-items: center;
                    display: flex;
                    font-size: 14px;
                    font-weight: bold;
                    height: 50px;
                    justify-content: center;
                    padding-bottom: 20px;
                }
            </style>

        </div>


    </div>
</div>

<?php get_footer();?>
