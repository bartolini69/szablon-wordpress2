<?php /* Template Name: Products Template */ get_header(); ?>

<div class="container break">

</div>

<div class="container" >
    <div class="row">
        <div class="col-md-3 text-center">
            <ul class="list-group menu-left">
                <li class="katalog list-group-item ">
                    <?php 
                        $lang = pll_current_language('slug'); 
                        if($lang == 'pl') { 
                    ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog.pdf') ?>"><p class="p1"><?php _e('POBIERZ NOWY') ?></p><p class="p2"><?php _e('KATALOG') ?></p> </a>
                    <?php } if($lang == 'fr') {  ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog_fr.pdf') ?>"><p class="p1"><?php _e('NOUVELLE') ?></p><p class="p2"><?php _e('OFFRE') ?></p> </a>
                    <?php } if($lang == 'en') { ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog_en.pdf') ?>"><p class="p1"><?php _e('NEW') ?></p><p class="p2"><?php _e('OFFER') ?></p> </a>
                    <?php } ?>
                    <div class="show-menu2">
                        <img src="<?php echo DEREN_THEME_URL ?>img/pdf_download.png" class="" alt=""/>
                    </div>
                </li>
                                <?php 
                    if($lang == 'fr') 
                        include('elements/menu-left-fr.php'); 
                    elseif($lang == 'en') 
                        include('elements/menu-left-en.php'); 
                    else
                        include('elements/menu-left.php'); 
                ?>
            </ul>
        </div>
        <div class="col-md-3 text-index">
            <div class="content-label text-center">
                <p class="h44">
                    <?php
                    $pagename = get_query_var('pagename');
                    if ( !$pagename && $id > 0 ) {
                        // If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
                        $post = $wp_query->get_queried_object();
                        $pagename = $post->post_name;
                    }
                    $title= $pagename;



                    $tradi='tradi';
                    $classic='classic';
                    $design='design';
                    $wew='wewnętrzne';

                    $klas='klasyczne';
                    $fra='system-francuski';
                    $eng='system-angielski';

                    if ($title==$tradi || $title==$classic || $title==$design || $title==$wew ):
                        echo 'DRZWI';
                    elseif ($title==$klas || $title==$fra || $title==$eng):
                        echo 'OKNA';
                    else:
                        echo '';
                    endif;
                    ?>
                </p>
                <p class="h11" style="line-height: 32px;font-size:30px;"><strong><?php the_title(); ?></strong></p>
            </div>
        </div>
        <div class="col-md-6">
            <br>
            <div class="content-label2">
            </div>
        </div>
        <div class="col-md-9 text-index product-holder">
            <div class="photo">
                <a href="<?php the_field('zdjecie'); ?>">
                	<img src="<?php the_field('zdjecie'); ?>" alt="">
                </a>
            </div>

            <div class="description">
                <div class="title"><?php the_field('tytul'); ?></div>
                <div class="content">
                	Opis:
                	<div class="margin">
                		<?php the_field('opis'); ?>
                	</div>
                </div>
                 <div class="technical">
                	Dane techniczne:
                	<div class="margin">
                		<?php the_field('dane_techniczne'); ?>
                	</div>
                </div> 
                 <div class="colors">
                	<div class="title">
                		Kolory
                	</div>
                	<img src="<?php the_field('kolory'); ?>" alt="">
                </div>              
            </div>

			<?php 
			$images = get_field('galeria');
			if( $images ): ?>
	            <div class="photos">
	            	<div class="title">	
	            		Inne zdjęcia:
	            	</div>
	            	<div class="photos-holder">
			        <?php foreach( $images as $image ): ?>
			                <a href="<?php echo $image['url']; ?>" class="fancybox photo">
			                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			                </a>
			        <?php endforeach; ?>
	            	</div>
	            </div>
			<?php endif; ?>
        </div>


    </div>
</div>

<?php get_footer();?>
