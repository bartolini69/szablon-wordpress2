<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="<?php bloginfo('charset')?>"/>

    <?php if(is_search()):?>
    <meta name="robots" content="noindex, nofollow" />
    <?php endif; ?>

    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>

    <meta http-equiv="Content-Type" content="text/html" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo DEREN_THEME_URL ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo DEREN_THEME_URL ?>css/style.css">
    <link rel="stylesheet" href="<?php echo DEREN_THEME_URL ?>css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Archivo Black" rel="stylesheet" type="text/css">

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" >

    <?php wp_head() ?>
    
</head>
<body <?php body_class()?>>

<div class="container-fluid">
    <div class="row">
        <div class="col-xl-1 logo col-xs-1">
            <a href="<?php echo esc_url(home_url('/'))?>">
                <img src="<?php echo DEREN_THEME_URL ?>img/menu_logo2.png" >
            </a>
        </div>
        <div class="col-md-10 banner">
            <div id="phone" class="container">
                <div class="row">
                    <div class="col-md-offset-9 ">
                        <p>
                        	<?php if(get_locale() == "pl_PL") { ?>
                            	<?php _e('<small>TEL.:</small> (17) 227 60 44') ?>
                            <?php } else { ?>
                            	33(6) 89 24 29 36
                            <?php } ?>
                            <a href="http://ystudio2.pl/test/deren2/pl/" ><img src="<?php echo DEREN_THEME_URL ?>img/menu_flaga_pl.png"></a>
                            <a href="http://ystudio2.pl/test/deren2/fr/" ><img src="<?php echo DEREN_THEME_URL ?>img/menu_flaga_fr.png"></a>
                            <a href="http://ystudio2.pl/test/deren2/en/" ><img src="<?php echo DEREN_THEME_URL ?>img/menu_flaga_gb.png"></a>

                        </p>
                    </div>
                </div>
            </div>

            <div class="container ">
                <?php _e('<div id="top">') ?>

                    <nav class="navbar navbar-inverse">
                        <?php


                        wp_nav_menu( array(
                                'menu'              => 'main_nav',
                                'theme_location'    => 'main_nav',
                                'depth'             => 3,
                                'container'         => '',
                                'container_class'   => '',
                                'container_id'      => '',
                                'menu_class'        => 'nav navbar-nav',
                                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                'after'             => '',
                                'walker'            => new wp_bootstrap_navwalker()
                        ));
                        ?>


                            <div class="show-carousel">
                                <div class="carousel slide media-carousel" id="media" style = "width:100%">
                                    <div class="carousel-inner">
                                        <div class="item  active">
                                            <div class="row" >
                                                <div class="col-md-4" style = "width:100%">
                                                    <a class="shadows" href="#"><img src="<?php echo DEREN_THEME_URL ?>img/menu_door.png" alt=""/> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-md-4"  style = "width:100%">
                                                    <a class="shadows" href="#"> <img src="<?php echo DEREN_THEME_URL ?>img/menu_door2.png" alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="row">

                                                <div class="col-md-4"  style = "width:100%">
                                                    <a class="shadows" href="#"> <img src="<?php echo DEREN_THEME_URL ?>img/menu_door3.png" alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="row">

                                                <div class="col-md-4"  style = "width:100%">
                                                    <a class="shadows" href="#"> <img src="<?php echo DEREN_THEME_URL ?>img/menu_door4.png" alt=""/></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a data-slide="prev" href="#media" class="left carousel-control"></a>
                                    <a data-slide="next" href="#media" class="right carousel-control"></a>
                                </div>
                            </div>



                        </ul>

                    </nav>
                </div>
            </div>
        </div>
    </div>