<?php get_header();?>

<div class="container break">

</div>
        <div class="col-md-6 text-index">
            <div id="contact" class="container" >
                <div class="row">
                    <div class="col-md-3 text-center">
                        <ul class="list-group menu-left">
                <li class="list-group-item title">
                    <a href=""><?php _e(' DRZWI ') ?> </a>
                </li>
                                <?php 
                    if($lang == 'fr') 
                        include('elements/menu-left-fr.php'); 
                    elseif($lang == 'en') 
                        include('elements/menu-left-en.php'); 
                    else
                        include('elements/menu-left.php'); 
                ?>
            </ul>
                    </div>
                    <div class="col-md-6 text-index">
                        <p><strong>Zakład usługowy - stolarstwo budowlane <br>Jan Dereń</strong></p>
                        <br>
                        <br>

                        <div class="col-md-6 ">
                            <p> Wola Mielecka 710</p>
                            <p> 39-300 Mielec</p>
                            <p> tel. 17 227 60 44</p>
                            <p> kom. 608 553 753</p>
                            <br>
                            <p><a class="mail" href="mailto:biuro@deren.com.pl"> biuro@deren.com.pl</a> </p>
                        </div>
                        <div class="col-md-6 pad">
                            <p></p>
                            <p>
                                <img src="<?php echo DEREN_THEME_URL ?>img/france.png" alt="france"/>   <img src="<?php echo DEREN_THEME_URL ?>img/uk.png" alt="united kingdom"/>
                            </p>
                            <p> David Kruzel</p>
                            <p> tel. 33(6) 89 24 29 36</p>
                            <br>
                            <p><a class="mail" href="mailto:contact@deren.com.pl"> contact@deren.com.pl</a> </p>
                        </div>
                        <p>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2539.274565042729!2d21.541539416027916!3d50.47323207947851!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473d46512b6a3bfd%3A0x6dde4a69263d897c!2sDere%C5%84+J.+ZU.+Stolarstwo+budowlane!5e0!3m2!1spl!2spl!4v1478526587038" width="500" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </p>
                        <br>
                        <br>

                    </div>
                    <div class="col-md-3 ">
                        <form id="contact-form" method="post" action="contact.php" role="form">
                            <br>
                            <br>
                            <p><strong>Formularz kontaktowy</strong></p>
                            <div class="messages"></div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="form_name" name="name" placeholder="Imię i nazwisko" required="required" data-error="Proszę, wpisz imię i nazwisko!"/>
                                <span class="help-block with-errors" ></span>
                            </div>
                            <div class="form-group">

                                <input type="email" class="form-control" id="form_email" name="email" placeholder="Adres e-mail" required="required" data-error="Proszę, wpisz adres e-mail!"/>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <input type="tel" class="form-control" id="form_phone" name="phone" placeholder="Temat" required="required" data-error="Proszę, wpisz temat wiadomości!"/>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <textarea rows="5" cols="30" class="form-control" id="form_message" name="message" placeholder="Wiadomość" required="required" data-error="Proszę, wpisz wiadomość!" ></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <button type="submit" class="btn btn-block btn-send btn-lg" >WYŚLIJ WIADOMOŚĆ!</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <ul class="list-group glowna-pobierz">
                <li class="katalog list-group-item ">
                    <?php 
                        $lang = pll_current_language('slug'); 
                        if($lang == 'pl') { 
                    ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog.pdf') ?>"><p class="p1"><?php _e('POBIERZ NOWY') ?></p><p class="p2"><?php _e('KATALOG') ?></p> </a>
                    <?php } if($lang == 'fr') {  ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog_fr.pdf') ?>"><p class="p1"><?php _e('NOUVELLE') ?></p><p class="p2"><?php _e('OFFRE') ?></p> </a>
                    <?php } if($lang == 'en') { ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog_en.pdf') ?>"><p class="p1"><?php _e('NEW') ?></p><p class="p2"><?php _e('OFFER') ?></p> </a>
                    <?php } ?>
                    <div class="show-menu2">
                        <img src="<?php echo DEREN_THEME_URL ?>img/pdf_download.png" class="" alt=""/>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php get_footer();?>
