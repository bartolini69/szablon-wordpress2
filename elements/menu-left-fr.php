<li class="list-group-item title">
                    <a href=""><?php _e(' PORTES ') ?> </a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>classic/"><?php _e(' CLASSIC') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/classic.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>drzwi-tradi/"> <?php _e(' TRADI ') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/tradi.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>design/"> <?php _e(' DESIGN ') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/design.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>wewnetrzne/"> <?php _e(' INTERIEURES ') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img\wewnetrzne.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <br>
                <li class="list-group-item title">
                    <a href=""><?php _e(' FENETRES ') ?> </a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>klasyczne/"><?php _e(' STANDARD ') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/okno3.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>system-francuski/"> <?php _e('SYSTEME FRANÇAIS') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/okno1.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>system-angielski/"> <?php _e('SYSTEME ANGLAIS') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/okno2.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <br>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>akcesoria/"> <?php _e('ACCESSOIRES') ?></a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/akcesoria.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <br>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>promocje/"> <?php _e('OFFRES SPECIALES') ?></a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/sale.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>