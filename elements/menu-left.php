<li class="list-group-item title">
                    <a href=""><?php _e(' DRZWI ') ?> </a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>classic/"><?php _e(' CLASSIC') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/classic.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>drzwi-tradi/"> <?php _e(' TRADI ') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/tradi.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>design/"> <?php _e(' DESIGN ') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/design.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>wewnetrzne/"> <?php _e(' WEWNĘTRZNE ') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img\wewnetrzne.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <br>
                <li class="list-group-item title">
                    <a href=""><?php _e(' OKNA ') ?> </a>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>klasyczne/"><?php _e(' KLASYCZNE ') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/okno3.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>system-francuski/"> <?php _e('SYSTEM FRANCUSKI') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/okno1.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>system-angielski/"> <?php _e('SYSTEM ANGIELSKI') ?> </a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/okno2.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <br>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>akcesoria/"> <?php _e('AKCESORIA') ?></a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/akcesoria.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>
                <br>
                <li class="list-group-item">
                    <a href="<?php echo PAGE_URL ?>promocje/"> <?php _e('PROMOCJE') ?></a>
                    <div class="show-menu">
                        <img src="<?php echo DEREN_THEME_URL ?>img/sale.png" class="menu-img-rounded" alt=""/>
                    </div>
                </li>