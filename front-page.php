<?php get_header();?>

<div class="container break">

</div>

<div id="middle" class="container" >
    <div class="row">
        <div class="col-md-3 text-center">
            <ul class="list-group menu-left">
                <?php 
                    if($lang == 'fr') 
                        include('elements/menu-left-fr.php'); 
                    elseif($lang == 'en') 
                        include('elements/menu-left-en.php'); 
                    else
                        include('elements/menu-left.php'); 
                ?>
            </ul>
        </div>
        <div class="col-md-6 text-index">
            <?php _e('
                <p>
                    Mamy zaszczyt zaprezentować Państwu kolekcję drzwi DEREN, całą gamę modeli zewnętrznych i wewnętrznych. Jesteśmy przekonani, że Nasza oferta zaspokoi potrzeby klientów szukających wyrobów o wysokiej jakości wykonania i niepowtarzalnej formie. Kolekcja wewnętrznych drzwi DEREN to gama nieprzeciętnych rozwiązań, doskonale pasujących do każdego wnętrza.
                </p>

                <p>
                    Nasze drzwi to nie tylko funkcjonalny dodatek, to również element architektury wnętrza, doskonale podkreślający jego charakter i klimat. A&nbsp;wszystko to w przystępnej cenie. Drzwi firmy DEREN to gwarancja jakości i niepowtarzalny styl.
                </p>
                <p>
                    DEREN to nie tylko drzwi, w Naszej ofercie znaleźć można również okna, balustrady, płoty i wiele wiele innych, więcej informacji można uzyskać pod nr telefonu: <strong>17 / 227 60 44.</strong>
                </p>
                ') ?>
        </div>
        <div class="col-md-3">
            <ul class="list-group glowna-pobierz">
                <li class="katalog list-group-item ">
                    <?php 
                        $lang = pll_current_language('slug'); 
                        if($lang == 'pl') { 
                    ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog.pdf') ?>"><p class="p1"><?php _e('POBIERZ NOWY') ?></p><p class="p2"><?php _e('KATALOG') ?></p> </a>
                    <?php } if($lang == 'fr') {  ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog_fr.pdf') ?>"><p class="p1"><?php _e('NOUVELLE') ?></p><p class="p2"><?php _e('OFFRE') ?></p> </a>
                    <?php } if($lang == 'en') { ?>
                    <a href="<?php echo PAGE_URL ?><?php _e('katalog_en.pdf') ?>"><p class="p1"><?php _e('NEW') ?></p><p class="p2"><?php _e('OFFER') ?></p> </a>
                    <?php } ?>
                    <div class="show-menu2">
                        <img src="<?php echo DEREN_THEME_URL ?>img/pdf_download.png" class="" alt=""/>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php get_footer();?>
