<?php
if(!defined('DEREN_THEME_DIR'))
define('DEREN_THEME_DIR', get_theme_root().'/'.get_template().'/');

if(!defined('DEREN_THEME_URL'))
    define('DEREN_THEME_URL', WP_CONTENT_URL.'/themes/'.get_template().'/');

if(!defined('PAGE_URL'))
    define('PAGE_URL', site_url().'/');


require_once ('libs/wp_bootstrap_navwalker.php');

function register_my_menus(){
    register_nav_menus(array(
        'main_nav' => __( 'Górne Menu', 'deren' ),
        'footer_nav' => __( 'Dolne Menu', 'deren' ),
    ) );
}

add_action('init','register_my_menus');


add_action( 'init', 'produkty' );

function produkty() {
    $labels =         
        array(
           'name' => __( 'Produkty' ),
           'singular_name' => __( 'Produkt' ),
           'add_new' => __( 'Dodaj nowy produkt' ),
           'add_new_item' => __( 'Dodaj nowy produkt' ),
           'edit_item' => __( 'Edytuj produkt' ),
           'new_item' => __( 'Dodaj produkt' ),
           'view_item' => __( 'Pokaż produkt' ),
           'search_items' => __( 'Szukaj produktu' ),
           'not_found' => __( 'Produkt nie odnaleziony' ),
           'not_found_in_trash' => __( 'Brak produktów w koszu' )
        );

    $args = 
        array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite' => array('slug' => 'produkty'),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports' => array('title', 'custom-fields', 'revisions', 'trackbacks')         

        );

    register_post_type( 'produkty', $args );
}

