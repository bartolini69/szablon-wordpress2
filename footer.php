<div class="container">
    <div id="bottom">
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <nobr><p class="partners"><?php _e('Współpracujemy z:') ?></p></nobr>

                        <img src="<?php echo DEREN_THEME_URL ?>img/partners.png" class="partners-img " alt=""/>
                    </div>
                    <div class="col-md-3 text-center flex-center-vertically">
                        <p class="h5">
                            Zakład Usługowy - Stolarstwo <br>
                            Budowlane Jan Dereń
                        </p>

                        <p class="h6">
                            Wola Mielecka 710 <br>
                            39-300 Mielec
                        </p>

                        <p class="h4">
                            tel. 17 227 60 44 <br>
                            <?php if(get_locale() == "pl_PL") { ?>
                            kom. 608 553 753
                            <?php } ?>
                        </p>

                        <p class="h4">
                            biuro@deren.com.pl
                        </p>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <nav class="navbar navbar-inverse fotbar">
                            <?php


                            wp_nav_menu( array(
                                    'menu'              => 'footer_nav',
                                    'theme_location'    => 'footer_nav',
                                    'depth'             => 2,
                                    'container'         => '',
                                    'container_class'   => '',
                                    'container_id'      => '',
                                    'menu_class'        => 'nav navbar-nav fotbar-nav',
                                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                    'after'             => '<li><i class="fa fa-fot fa-circle" aria-hidden="true"></i></li>',

                                    'walker'            => '')
                            );
                            ?>

                            <!--
                            <ul class="nav navbar-nav fotbar-nav">
                                <li class="nav-item fot-item">
                                    <a class="nav-link" href="index.html">STRONA GŁÓWNA </a>
                                </li>
                                <li><i class="fa fa-fot fa-circle" aria-hidden="true"></i></li>
                                <li class="nav-item fot-item">
                                    <a class="nav-link" href="oferta.html">OFERTA</a>
                                </li>
                                <li><i class="fa fa-fot fa-circle" aria-hidden="true"></i></li>
                                <li class="nav-item fot-item">
                                    <a class="nav-link" href="technika.html">TECHNIKA</a>
                                </li>
                                <li><i class="fa fa-fot fa-circle" aria-hidden="true"></i></li>
                                <li class="nav-item fot-item">
                                    <a class="nav-link" href="ofirmie.html">O FIRMIE</a>
                                </li>
                                <li><i class="fa fa-fot fa-circle" aria-hidden="true"></i></li>
                                <li class="nav-item fot-item">
                                    <a class="nav-link" href="kontakt.html">KONTAKT</a>
                                </li>
                            </ul>
                            -->
                        </nav>
                    </div>
                </div>

            </div>
        </div>
        <a href="http://www.ystudio.pl"><img src="<?php echo DEREN_THEME_URL ?>img/ystudio.png" class="img-logo img-responsive" alt="ystudio"/></a>
    </div>
</div>


<script src="<?php echo DEREN_THEME_URL ?>js/bootstrap.min.js"></script>

<script>
    jQuery(function() {
        /**
         * NAME: Bootstrap 3 Multi-Level by Johne
         * This script will active Triple level multi drop-down menus in Bootstrap 3.*
         */
        jQuery('li.dropdown-submenu').on('click', function(event) {
            event.stopPropagation();
            if (jQuery(this).hasClass('open')){
                jQuery(this).removeClass('open');
            }else{
                jQuery('li.dropdown-submenu').removeClass('open');
                jQuery(this).addClass('open');
            }
        });
    });

</script>

<script>
    jQuery(document).ready(function() {
        jQuery('#media').carousel({

            pause: true,
            interval: false
        });
    });
</script>

</div>
</body>
</html>