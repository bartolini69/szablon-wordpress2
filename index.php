<?php get_header();?>

    <div class="container break">

    </div>

    <div id="middle" class="container" >
        <div class="row">
            <div class="col-md-3 text-center">
                <ul class="list-group menu-left">
                    <li class="list-group-item">
                        <a href="#"> <?php _e('DRZWI CLASSIC') ?> </a>
                        <div class="show-menu">
                            <img src="<?php echo DEREN_THEME_URL ?>img/classic.png" class="menu-img-rounded" alt=""/>
                        </div>

                    </li>
                    <li class="list-group-item">
                        <a href="#"> <?php _e('DRZWI TRADI') ?>  </a>
                        <div class="show-menu">
                            <img src="<?php echo DEREN_THEME_URL ?>img/menu_door3.png" class="menu-img-rounded" alt=""/>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <a href="#"> <?php _e('DRZWI DESIGN') ?>  </a>
                        <div class="show-menu">
                            <img src="<?php echo DEREN_THEME_URL ?>img/menu_door.png" class="menu-img-rounded" alt=""/>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <a href="#"> <?php _e('DRZWI WEWNĘTRZNE') ?>  </a>
                        <div class="show-menu">
                            <img src="<?php echo DEREN_THEME_URL ?>img/menu_door4.png" class="menu-img-rounded" alt=""/>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <a href="#"> <?php _e('OKNA') ?>  </a>
                        <div class="show-menu">
                            <img src="<?php echo DEREN_THEME_URL ?>img/okno.png" class="menu-img-rounded" alt=""/>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <a href="#"> <?php _e('AKCESORIA') ?>  </a>
                        <div class="show-menu">
                            <img src="<?php echo DEREN_THEME_URL ?>img/akcesoria.png" class="menu-img-rounded" alt=""/>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 text-index">
                <?php _e('
                <p>
                    Mamy zaszczyt zaprezentować Państwu kolekcję drzwi DEREN, całą gamę modeli zewnętrznych i wewnętrznych. Jesteśmy przekonani, że Nasza oferta zaspokoi potrzeby klientów szukających wyrobów o wysokiej jakości wykonania i niepowtarzalnej formie. Kolekcja wewnętrznych drzwi DEREN to gama nieprzeciętnych rozwiązań, doskonale pasujących do każdego wnętrza.
                </p>

                <p>
                    Nasze drzwi to nie tylko funkcjonalny dodatek, to również element architektury wnętrza, doskonale podkreślający jego charakter i klimat. A&nbsp;wszystko to w przystępnej cenie. Drzwi firmy DEREN to gwarancja jakości i niepowtarzalny styl.
                </p>
                <p>
                    DEREN to nie tylko drzwi, w Naszej ofercie znaleźć można również okna, balustrady, płoty i wiele wiele innych, więcej informacji można uzyskać pod nr telefonu: <strong>17 / 227 60 44.</strong>
                </p>
                ') ?>
            </div>
            <div class="col-md-3">
                <ul class="list-group glowna-pobierz">
                    <li class="katalog list-group-item ">
                        <a href="#"><p class="p1"> <?php _e('POBIERZ NOWY') ?></p><p class="p2"><?php _e('KATALOG') ?></p> </a>
                        <div class="show-menu2">
                            <img src="<?php echo DEREN_THEME_URL ?>img/pdf_download.png" class="" alt=""/>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

<?php get_footer();?>
